LOCAL_PATH := $(call my-dir)
CLANG_ROOT_PATH := $(LOCAL_PATH)/../clang
subdirs := $(addprefix $(LOCAL_PATH)/,$(addsuffix /Android.mk, \
  clang-tidy \
  ))
include $(CLANG_ROOT_PATH)/clang.mk
include $(subdirs)
