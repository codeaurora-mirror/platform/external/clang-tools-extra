LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libclangTidyCppCoreGuidelinesModule
LOCAL_MODULE_CLASS := STATIC_LIBRARIES

include $(LOCAL_PATH)/../Android.common.mk

LOCAL_SRC_FILES := $(sort $(notdir $(wildcard $(LOCAL_PATH)/*.cpp)))

LOCAL_STATIC_LIBRARIES := \
  libclangAST \
  libclangASTMatchers \
  libclangBasic \
  libclangLex \
  libclangTidy \
  libclangTidyMiscModule \
  libclangTidyUtils \
  libclangTooling \

include $(BUILD_HOST_STATIC_LIBRARY)
